#!/usr/bin/python

import xml.etree.ElementTree

import argparse

# Lets add some arguments...
parser = argparse.ArgumentParser()
parser.add_argument("--zonefile", help="firewalld XML zone file")
args = parser.parse_args()

def getSourceIp(xmlFile):
  element = xml.etree.ElementTree.parse(xmlFile).getroot()
  sourceIp = ""
  for ip in element.findall('source'):
    sourceIp = (ip.get('address').replace('/32',''))
  return sourceIp

if __name__ == "__main__":
    if args.zonefile:  # check "-cmd" option
        print getSourceIp(args.zonefile)
    else:
      parser.print_help()
