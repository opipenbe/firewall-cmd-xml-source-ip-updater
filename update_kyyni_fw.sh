#!/bin/bash

ZONE="kyyni"
FQDN="kyyni.pipenberg.eu"
IP_FROM_DNS=$(dig +short $FQDN)
IP_FROM_FW=$(python /root/firewall-cmd-xml-source-ip-updater/update_xml.py --zonefile=/etc/firewalld/zones/$ZONE.xml)

if [ "$IP_FROM_DNS" != "$IP_FROM_FW" ]; then
  logger "$FQDN IP is changed, updating ip in firewall!"
  echo "$FQDN IP is changed, updating ip in firewall!"
  firewall-cmd --remove-source=$IP_FROM_FW/32 --zone=$ZONE --permanent
  firewall-cmd --add-source=$IP_FROM_DNS/32 --zone=$ZONE --permanent
  firewall-cmd --reload
  exit 1
fi

exit 0
